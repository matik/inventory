<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('Barang_model');
	}

	public function index()
	{
		$data['barang'] = $this->Barang_model->get();
		$data['page'] = 'barang/daftar_barang';
		$this->load->view('layout', $data);
	}

	public function add($id = NULL)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('barang_name', 'Nama Barang', 'trim|required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button position="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');

		if ($_POST AND $this->form_validation->run() == TRUE) {

			if ($this->input->post('barang_id')) {
				$params['barang_id'] = $id;
			}
			$params['barang_name'] = $this->input->post('barang_name');
			$status = $this->Barang_model->add($params);

			redirect('barang');
		}else{
			// Edit mode
			if ($this->input->post('barang_id')) {
				redirect('barang/edit/' . $this->input->post('barang_id'));
			}
			if (!is_null($id)) {
				$data['barang'] = $this->Barang_model->get(array('id' => $id));
			}

		}
		$data['page'] = 'barang/tambah_barang';
		$this->load->view('layout', $data);
	}

	public function view($id = NULL)
	{
		$data['barang'] =  $this->Barang_model->get(array('id' => $id));
		$data['page'] = 'barang/view_barang';
		$this->load->view('layout', $data);
	}

	public function delete($id = NULL)
	{
		if($_POST){
			$this->Barang_model->delete($id);
			redirect('barang');
		}
		$data['barang'] =  $this->Barang_model->get(array('id' => $id));
		$data['page'] = 'barang/hapus_barang';
		$this->load->view('layout', $data);
	}
}
