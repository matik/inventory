<?php 

if(isset($barang)){
// Edit mode

	$id = $barang['barang_id'];
	$name = $barang['barang_name'];

}else{
// Add model
	$name = set_value('barang_name');

}

?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Tambah barang
			<small>Ini berisi daftar barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Barang</a></li>
			<li class="active">Tambah barang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Pembuka form -->
		<?php echo form_open(current_url()); ?>

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Tambah barang</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-9">
							<!-- Untuk menampilkan validasi jika error -->
							<?php echo validation_errors(); ?>

							<!-- Untuk mode edit  -->
							<?php if (isset($barang)) { ?>
							<input type="hidden" name="barang_id" value="<?php echo $id; ?>">
							<?php } ?>

							<div class="form-group">
								<label>Nama barang <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
								<input name="barang_name" required="" type="text" class="form-control" value="<?php echo $name ?>" placeholder="Nama barang">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Aksi</label>
								<button class="btn btn-success btn-flat btn-block">
									<span class="fa fa-check"></span> Simpan
								</button>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						Footer
					</div>
					<!-- /.box-footer-->
				</div>
				<!-- /.box -->

				<!-- Penutup Form -->
				<?php echo form_close() ?>

			</section>
			<!-- /.content -->
		</div>
					<!-- /.content-wrapper