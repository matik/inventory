<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar barang
			<small>Ini berisi daftar barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Barang</a></li>
			<li class="active">Daftar barang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Daftar barang</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>
										NAMA
									</th>
									<th>
										AKSI
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($barang as $row): ?>
									
								<tr>
									<td><?php echo $row['barang_name'] ?></td>
									<td>
										<a href="<?php echo site_url('barang/edit/'. $row['barang_id']) ?>" class="btn btn-success btn-xs btn-flat">
											<span class="glyphicon glyphicon-pencil"></span>
										</a>
										<a href="<?php echo site_url('barang/view/'. $row['barang_id']) ?>" class="btn btn-warning btn-xs btn-flat">
											<span class="glyphicon glyphicon-zoom-in"></span>
										</a>
										<a href="<?php echo site_url('barang/delete/'. $row['barang_id']) ?>" class="btn btn-danger btn-xs btn-flat">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</td>
								</tr>

								<?php endforeach ?>

							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						Footer
					</div>
					<!-- /.box-footer-->
				</div>
				<!-- /.box -->

			</section>
			<!-- /.content -->
		</div>
					<!-- /.content-wrapper -->