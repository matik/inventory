<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail barang
			<small>Ini berisi detail barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Barang</a></li>
			<li class="active">Detail barang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box">
			<!-- Pembuka form -->
			<?php echo form_open(site_url('barang/delete/'.$barang['barang_id'])); ?>
			<div class="box-header with-border">
				<h3 class="box-title">Detail barang</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-striped">
							<tbody>
								
								<tr>
									<td>Nama barang</td>
									<td>:</td>
									<td>
										<?php echo $barang['barang_name']; ?>
									</td>
								</tr>

							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<a href="<?php echo site_url('barang') ?>" class="btn btn-warning btn-flat">
							<span class="fa fa-close"></span> Batal
						</a>
						<button type="submit" class="btn btn-danger btn-flat">
							<span class="fa fa-check"></span> Hapus
						</button>
					</div>
					<!-- /.box-footer-->
					<?php echo form_close(); ?>
				</div>
				<!-- /.box -->

			</section>
			<!-- /.content -->
		</div>
					<!-- /.content-wrapper -->