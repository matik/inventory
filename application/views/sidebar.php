<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?php echo base_url() ?>media/img/user2-160x160.jpg" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Sistiandy</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview">
						<a href="<?php echo site_url() ?>">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>

						</a>
					</li>

					<li class="treeview <?php echo ($this->uri->segment(1) == 'barang') ? 'active' : '' ?>">
						<a href="#">
							<i class="fa fa-shopping-bag"></i> <span>Barang</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo site_url('barang') ?>"><i class="fa fa-circle-o"></i> Daftar Barang</a></li>
							<li><a href="<?php echo site_url('barang/add') ?>"><i class="fa fa-circle-o"></i> Tambah Barang</a></li>
						</ul>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>