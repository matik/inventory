<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * barang Model Class
 *
 * @package     SYSCMS
 * @subpackage  Models
 * @category    Models
 * @author      Sistiandy Syahbana nugraha <sistiandy.web.id>
 */
class barang_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['id'])) {
            $this->db->where('barang.barang_id', $params['id']);
        }

        $this->db->select('barang.barang_id, barang_name');

        $res = $this->db->get('barang');

        if (isset($params['id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    // Insert some data to table
    function add($data = array()) {

        if (isset($data['barang_id'])) {
            $this->db->set('barang_id', $data['barang_id']);
        }

        if (isset($data['barang_name'])) {
            $this->db->set('barang_name', $data['barang_name']);
        }

        if (isset($data['barang_id'])) {
            $this->db->where('barang_id', $data['barang_id']);
            $this->db->update('barang');
            $id = $data['barang_id'];
        } else {
            $this->db->insert('barang');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }
    
    // Drop some data to table
    function delete($id) {
        $this->db->where('barang_id', $id);
        $this->db->delete('barang');
    }

}
